# Sports poll app

> Full stack app with React/Redux and Express. The app shows a random sports event shows polling options for what team will win or it will be a draw.

## Quick Start

``` bash
# Install dependencies for server
npm install

# Install dependencies for client
npm run client-install

# Run the client & server with concurrently
npm run dev

# Run the Express server only
npm run server

# Run the React client only
npm run client

# Server runs on http://localhost:5000 and client on http://localhost:3000
```

## App Info

### Authors

Afzalur Rahman

### Version

1.0.0

### License

This project is licensed under the MIT License
"# Sports_Poll" 
"# Sports_poll_app" 
