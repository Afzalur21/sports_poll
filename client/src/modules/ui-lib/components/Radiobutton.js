import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './Radiobutton.css';

export class RadioButton extends React.Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    options: PropTypes.array.isRequired,
    onChange: PropTypes.func,
    onBlur: PropTypes.func,
    value: PropTypes.string.isRequired,
    orientation: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.onValueChange = this.onValueChange.bind(this);
    this.getItemClassName = this.getItemClassName.bind(this);
    this.state = {
      selectedOption: ''
    };
  }

  onValueChange = event => {
    // this.props.onBlur(event);
    this.setState({ selectedOption: event.target.value });    
  };

  handleOptionChange = () => {};

  getItemClassName(item) {
    return classNames(styles.Label, {
      [styles.Disabled]: item.disabled && true,
      [styles.ButtonVertical]: this.props.orientation === 'vertical',
      [styles.Checked]: this.props.value === item.value
    });
  }

  render() {
    return (
      <div
        className={classNames(styles.RadioContainer, {
          [styles.RadiobuttonVertical]: this.props.orientation === 'vertical'
        })}
      >
        {this.props.options.map((item, index) => {
          return (
            <label className={this.getItemClassName(item)} key={index}>
              <input
                type="radio"
                className={styles.RadioButton}
                name={this.props.name}
                value={item.value}
                disabled={item.disabled && true}
                onChange={this.onValueChange}
              />
              {item.text}
            </label>
          );
        })}
      </div>
    );
  }
}