import React, { Component } from 'react'
import { Provider } from 'react-redux'
import logo from './sports.jpg'
import './App.css'
import store from './store'
import Events from './components/Event/events'

class App extends Component {

  render () {
    return (
      <Provider store={ store }>
        <div className="App">
          <header className="App-header">
            <img src={ logo } className="App-logo" alt="logo"/>
            <h1 className="App-title">Vote for your favourite team</h1>
          </header>
          <Events/>
        </div>
      </Provider>
    )
  }
}

export default App
