import {GET_EVENTS} from '../actions/constants'
const eventReducer = (state = {}, {type, payload}) => {
    switch (type) {
      case GET_EVENTS:
        return payload
      default:
        return state
    }
}

export default eventReducer;