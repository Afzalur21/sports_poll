import {GET_EVENTS} from './constants';

export const getEvents = () => dispatch => {
  return fetch('/api/events')
    .then(res => res.json())
    .then(events => dispatch(
      {
        type: GET_EVENTS, 
        payload: events
      }
  ))
}