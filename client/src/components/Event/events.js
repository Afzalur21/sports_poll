import React, { Component } from 'react';
import PropTypes from 'prop-types'
import {connect } from 'react-redux';
import {getEvents} from '../../store/actions/event'
import './events.css';
import { RadioButton } from '../../modules/ui-lib/components/Radiobutton'
import logoFootball from '../../images/football.jpg'
import logoHandball from '../../images/handball.jpg'
import logoIceHockey from '../../images/ice-hockey.jpg'
import logoSnooker from '../../images/snooker.jpg'
import logoTennis from '../../images/tennis.jpg'
// var localStorage = require('localStorage')

// let eventName = '';

class Events extends Component {  

  static propTypes = {
    getEvents: PropTypes.func.isRequired,
    events: PropTypes.object.isRequired
  }

  constructor(props) {
    super(props);
    this.state = {
      poll_option: ''
    }
  }

  static defaultProps = {
    events: {},
    pollOption: ''
  }

  // storeIntoLocalStorage = (e) => {
  //   //read the previous poll count for the event and team, increment by one to store the updated poll count
  //   let pollCount = localStorage.getItem(eventName + '`' + e.target.value);
  //   pollCount = pollCount?pollCount:0;
  //   localStorage.setItem(eventName+ '`' + e.target.value, ++pollCount);
  // }

  componentWillMount() {
    this.props.getEvents();

  }

  render() {
    const event = this.props.events;
    // eventName = event.name;
    let categoryLogo = '';
    const pollOptions = [
      { 
        value: 'TEAMA', 
        text: 'Home Team win(Team A)' 
      },
      { 
        value: 'DRAW', 
        text: 'Draw' 
      },
      { 
        value: 'TEAMB', 
        text: 'Away Team win(Team B)' 
      }
    ];
    //set event category specific logo
    if(event.sport === 'FOOTBALL') {
      categoryLogo = logoFootball
    } else if(event.sport === 'HANDBALL') {
      categoryLogo = logoHandball
    } else if(event.sport === 'SNOOKER') {
      categoryLogo = logoSnooker
    } else if(event.sport === 'ICE_HOCKEY') {
      categoryLogo = logoIceHockey
    } else if(event.sport === 'TENNIS') {
      categoryLogo = logoTennis
    }


    return (
      <div>
        <h2>Who will win?</h2>
        <ul> 
        <li key={event.id}>
        <img src={ categoryLogo } className="Event-logo" alt="categoryLogo"/>
            <div>
            {event.name}
            </div>            
            <div>
                      <div style={{float:'center'}}>
                        <RadioButton name='poll_option' options={pollOptions} orientation="vertical" value={this.state.poll_option}
                        // onChange={this.storeIntoLocalStorage.bind(this)}
                        />
                      </div>  
              </div>
        </li>
        </ul>        
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  events: state.events
})

const dispatchToProps = (dispatch) => ({
   getEvents: () => dispatch(getEvents())
})

export default connect(mapStateToProps, dispatchToProps)(Events);