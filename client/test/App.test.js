import React from 'react';
import ReactDOM from 'react-dom';
// import App from '../App.js';
import App from '../src/App.js';
// import assert from 'assert';
const assert = require('chai').assert;



it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
});

describe('App', function(){
  describe('getEvents()', function(){
    it('The api should return only one event at a time', function(){
      return fetch('/api/events')
      .then(res => res.json())
      .then(events => {
        assert.equal(events.length, 1);
      });
    });

  });
});